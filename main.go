package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	var text string
	text = os.Args[1]

	dat, err := ioutil.ReadFile(text)
	check(err)

	var transferedText string
	transferedText = transferText(string(dat))

	fmt.Println("==== ORIGINAL TEXT ====")
	fmt.Println(string(dat))
	fmt.Println("==== TRANSFERED TEXT ====")
	fmt.Println(transferedText)

	error := ioutil.WriteFile(text+".leet", []byte(transferedText), 0664)
	check(error)
}

func checkText(input string) bool {
	if input == "" {
		return false
	}
	return true
}

func changeChar(input string) string {
	modifiedInput := strings.ToLower(input)

	rand := rand.Intn(1000)
	if rand > 500 {
		return input
	} else {

		switch modifiedInput {
		case "i":
			return "1"
		case "e":
			return "3"
		case "a":
			return "4"
		case "s":
			return "5"
		case "o":
			return "0"
		default:
			return input

		}
	}
}

func transferText(text string) string {
	var transfered string
	for _, c := range text {
		transfered += changeChar(string(c))
	}
	return transfered
}
